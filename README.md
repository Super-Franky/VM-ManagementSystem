## Linux下虚拟环境指标监控

1. 显示监控**虚拟机列表**和**服务器列表**，显示虚拟机和服务器之间**关系的图表**。

2. 虚拟机详情页实时显示各个资源的**占用率**，可显示各占用率的**历史情况折线图**。

    具体包括：内存占用率，cpu占用率，磁盘空间占用率及网络占用率



## 技术架构

> 详细版本请参阅dockerfile以及docker-compose文件

* 前端：
  * 框架：Vue
  * UI：Element-UI

* 后端
  * Django

* 数据库
  * InfluxDB
  * MariaDB


## 项目部署
> 本项目使用docker容器技术构建，使用docker-compose编排多容器
1. 使用git命令将仓库克隆到本地

   ```shell
   git clone https://gitlab.com/Super-Franky/VM-ManagementSystem.git
   ```

2. 进入前端文件夹下（src/frontend）执行命令

   ```
   npm install
   ```

3. 回到仓库根目录下，执行

   ```shell
   docker compose build
   //若是在linux下则执行 docker-compose build
   docker compose up -d
   //若是在linux下则执行 docker-compose up -d
   
   ```

4. 完成容器创建后进入后端容器（backend），执行

   ```shell
   python manage.py makemigrations 
   python manage.py migrate
   python manage.py makemigrations demo
   python manage.py migrate
   ```

5. 进入 influxdb 容器，执行

   ```shell
   influx
   CREATE DATABASE "vm_data"
   CREATE RETENTION POLICY "one_month" ON "vm_data" DURATION 720h REPLICATION 1 DEFAULT
   CREATE CONTINUOUS QUERY "cq_30m" ON "vm_data" BEGIN SELECT mean("mem_usage") AS "mean_mem_usage",mean("cpu_usage") AS "mean_cpu_usage",mean("disk_usage") AS "mean_disk_usage",mean("net_rx") AS "mean_net_rx",mean("net_tx") AS "mean_net_tx" INTO "autogen"."vm_30m" FROM "vm_5s" GROUP BY time(30m),vm_id END
   ```
6. 即可正常访问 localhost：8080

