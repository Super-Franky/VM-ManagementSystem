import axios from 'axios'
import { ElMessage } from 'element-plus'
import router from "@/router";

// 创建一个 axios 实例
const service = axios.create({
    timeout: 60000, // 请求超时时间毫秒
    withCredentials: true, // 异步请求携带cookie
    changeOrigin: true,    //是否跨域
    headers: {
        // 设置后端需要的传参类型
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
    },
})

// 添加请求拦截器
service.interceptors.request.use(
    function (config) {
        // 用于从cookie中匹配 csrftoken值
        let regex = /.*csrftoken=([^;.]*).*$/;
        // 将csrftoken值放到headers中，以通过django后端的校验
        config.headers['X-CSRFToken'] = document.cookie.match(regex) === null ? null : document.cookie.match(regex)[1];
        return config;
    },
    function (error) {
        // 对请求错误做些什么
        ElMessage.error('网络错误，请重试')
        return Promise.reject(error);
    }
)

// 添加响应拦截器
service.interceptors.response.use(
    function (response) {
        // 2xx 范围内的状态码都会触发该函数。
        // 如果返回错误，则提示
        if (response.data.code === 1) {
            ElMessage.error(response.data.msg);
        } else if (response.data.code === 2) { // 返回代码为2，表示未登录
            ElMessage.error("请先登录");
            router.push('/').then();
        }
        // dataAxios 是 axios 返回数据中的 data
        return response.data;
    },
    function (error) {
        // 超出 2xx 范围的状态码都会触发该函数。
        // 对响应错误做点什么
        ElMessage.error('网络错误，请重试')
        return Promise.reject(error);
    }
)

export default service;

