import httpRequest from '@/request/index'

// 获取用户的服务器列表
export function getServers() {
    return httpRequest({
        url: 'server/get/',
        method: 'get',
    })
}

// 获取对应服务器
export function getServerById(serverId) {
    return httpRequest({
        url: 'server/get_by_id/',
        method: 'post',
        data: {
            server_id: serverId,
        }
    })
}

// 测试连接
export function testConnect(server) {
    return httpRequest({
        url: 'server/test_access/',
        method: 'post',
        data: {
            ip: server.ip,
            alias: server.alias,
            username: server.username,
            password: server.password,
        },
    })
}

// 添加服务器
export function addServer(server) {
    return httpRequest({
        url: 'server/add/',
        method: 'post',
        data: {
            ip: server.ip,
            alias: server.alias,
            username: server.username,
            password: server.password,
        },
    })
}

// 删除服务器
export function deleteServer(serverId) {
    return httpRequest({
        url: 'server/delete/',
        method: 'delete',
        data: {
            server_id: serverId,
        },
    })
}

// 修改服务器别名
export function modifyAlias(serverId, newAlias) {
    return httpRequest({
        url: 'server/save_alias/',
        method: 'post',
        data: {
            server_id: serverId,
            new_alias: newAlias,
        },
    })
}
