import httpRequest from '@/request/index'

// 用户登录
export function login(username, password) {
    return httpRequest({
        url: 'account/login/',
        method: 'post',
        data: {
            username,
            password
        },
    })
}

// 用户登出
export function logout() {
    return httpRequest({
        url: 'account/logout/',
        method: 'post',
    })
}

// 用户注册
export function register(username, password) {
    return httpRequest({
        url: 'account/register/',
        method: 'post',
        data: {
            username,
            password
        },
    })
}
