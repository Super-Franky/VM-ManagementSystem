import httpRequest from '@/request/index'

// 获取对应服务器的虚拟机列表
export function getVMs(serverId) {
    return httpRequest({
        url: 'vm/get/',
        method: 'post',
        data: {
            server_id: serverId,
        }
    })
}

// 获取对应虚拟机
export function getVmById(vmId) {
    return httpRequest({
        url: 'vm/get_by_id/',
        method: 'post',
        data: {
            vm_id: vmId,
        }
    })
}

// 测试连接
export function testConnect(serverId, vm) {
    return httpRequest({
        url: 'vm/test_access/',
        method: 'post',
        data: {
            server_id: serverId,
            port: vm.port,
            alias: vm.alias,
            username: vm.username,
            password: vm.password,
        },
    })
}

// 添加虚拟机
export function addVM(serverId, vm) {
    return httpRequest({
        url: 'vm/add/',
        method: 'post',
        data: {
            server_id: serverId,
            port: vm.port,
            alias: vm.alias,
            username: vm.username,
            password: vm.password,
        },
    })
}


// 删除虚拟机
export function deleteVM(vmId) {
    return httpRequest({
        url: 'vm/delete/',
        method: 'delete',
        data: {
            vm_id: vmId,
        },
    })
}


// 获取5s间隔的数据
export function getData5s(vmId) {
    return httpRequest({
        url: 'vm/get_data_5s/',
        method: 'post',
        data: {
            vm_id: vmId,
        },
    })
}

// 获取30m间隔数据
export function getData30m(vmId) {
    return httpRequest({
        url: 'vm/get_data_30m/',
        method: 'post',
        data: {
            vm_id: vmId,
        },
    })
}

// 修改虚拟机别名
export function modifyAlias(vmId, newAlias) {
    return httpRequest({
        url: 'vm/save_alias/',
        method: 'post',
        data: {
            vm_id: vmId,
            new_alias: newAlias,
        },
    })
}

