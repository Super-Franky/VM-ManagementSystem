import { createRouter,createWebHistory } from "vue-router";
import LoginPage from "@/pages/LoginPage";
import RegisterPage from "@/pages/RegisterPage";
import HomePage from "@/pages/HomePage";
import ServerPage from "@/pages/ServerPage";
import VMPage from "@/pages/VMPage";
import OverviewPage from "@/pages/OverviewPage";
const router = createRouter({
    history:createWebHistory(),
    routes:[
        {
            path: '/',
            component: LoginPage,
        },
        {
            path: '/register',
            component: RegisterPage,
        },
        {
            path: '/home',
            component: HomePage,
        },
        {
            path: '/server',
            component: ServerPage,
        },
        {
            path: '/virtualMachine',
            component: VMPage,
        },
        {
            path: '/overview',
            component: OverviewPage,
        },
    ]
})
export default router;
