const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: 'http://backend:8000/demo'
    // proxy: 'http://localhost:8000/demo'
  }
})
