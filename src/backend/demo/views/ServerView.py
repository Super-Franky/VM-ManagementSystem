from django.db import transaction
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from demo.common.R import R
from demo.dto.ServerDto import ServerDto
from demo.models import Server, UserServer, UserVm, VirtualMachine
from demo.serializers import ServerSerializer, ServerDtoSerializer
from demo.service.server_service import ServerService


class ServerView(viewsets.ModelViewSet):
    queryset = Server.objects.all()
    serializer_class = ServerSerializer

    @action(methods=['get'], detail=False)
    def get(self, request):
        user_id = request.user.id
        try:
            user_servers = UserServer.objects.filter(user_id=user_id)
            servers = []
            for user_server in user_servers:
                servers.append(ServerDto(Server.objects.get(id=user_server.server_id), user_server))
            return Response(R.ok(ServerDtoSerializer(servers, many=True).data))
        except Exception as e:
            return Response(R.failed('获取失败'))

    @action(methods=['post'], detail=False)
    def get_by_id(self, request):
        server_id = request.data.get("server_id")
        user_id = request.user.id
        try:
            user_server = UserServer.objects.get(user_id=user_id, server_id=server_id)
            server = ServerDto(Server.objects.get(id=user_server.server_id), user_server)
            return Response(R.ok(ServerDtoSerializer(server).data))
        except Exception as e:
            return Response(R.failed('获取失败'))

    @action(methods=['post'], detail=False)
    def test_access(self, request):
        ip = request.data.get('ip')
        alias = request.data.get('alias')
        username = request.data.get('username')
        password = request.data.get('password')
        server_service = ServerService(ip, alias, username, password)
        auth = server_service.auth()
        if auth:
            return Response(R.ok('测试连接成功'))
        else:
            return Response(R.failed('测试链接失败'))

    @action(methods=['post'], detail=False)
    def add(self, request):
        user_id = request.user.id
        ip = request.data.get('ip')
        alias = request.data.get('alias')
        username = request.data.get('username')
        password = request.data.get('password')
        # 判断该服务器是否已经被该用户添加过
        target_server = Server.objects.filter(ip=ip, username=username, password=password).first()
        if target_server is not None and \
                UserServer.objects.filter(user_id=user_id, server_id=target_server.id).count() > 0:
            return Response(R.failed('该服务器已经被添加过'))
        server_service = ServerService(ip, alias, username, password)
        auth = server_service.auth()
        if auth:
            try:
                with transaction.atomic():
                    if target_server is None:
                        target_server = Server.objects.create(ip=ip, username=username, password=password)
                        target_server.save()
                    new_user_server = UserServer.objects.create(user_id=request.user.id, server_id=target_server.id, alias=alias)
                    new_user_server.save()
                    return Response(R.ok('添加成功'))
            except Exception:
                return Response(R.failed('添加失败，请重试'))
        else:
            return Response(R.failed('添加失败，请检查信息'))

    @action(methods=['delete'], detail=False)
    def delete(self, request):
        user_id = request.user.id
        server_id = request.data.get('server_id')
        try:
            with transaction.atomic():
                # 检查该服务器下是否还有虚拟机
                target_user_server = UserServer.objects.get(user_id=user_id, server_id=server_id)
                if target_user_server.vm_count > 0:
                    return Response(R.failed('该服务器下还有虚拟机，请先删除虚拟机'))

                UserServer.objects.get(user_id=user_id, server_id=server_id).delete()
                if UserServer.objects.filter(server_id=server_id).count() == 0:
                    Server.objects.get(id=server_id).delete()
                return Response(R.ok('删除成功'))
        except Exception:
            return Response(R.failed('删除失败，请重试'))

    @action(methods=['post'], detail=False)
    def save_alias(self, request):
        user_id = request.user.id
        server_id = request.data.get('server_id')
        new_alias = request.data.get('new_alias')
        target_server = UserServer.objects.get(user_id=user_id, server_id=server_id)
        target_server.alias = new_alias
        target_server.save()
        return Response(R.ok('修改成功'))
