import threading

from django.db import transaction
from django.http import HttpResponse
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from demo.common.R import R
from demo.common.check import check
from demo.dto.VmDto import VmDto
from demo.models import VirtualMachine, UserVm, Server, UserServer
from demo.serializers import VirtualMachineSerializer, VmDtoSerializer
from demo.service.vm_service import VmService


class VirtualMachineView(viewsets.ModelViewSet):
    queryset = VirtualMachine.objects.all()
    serializer_class = VirtualMachineSerializer

    # 虚拟机 map，id -》 vm_service
    vms = dict()

    @action(methods=['post'], detail=False)
    def get(self, request):
        user_id = request.user.id
        server_id = request.data.get('server_id')
        try:
            user_vms = UserVm.objects.filter(user_id=user_id)
            vms = []
            for user_vm in user_vms:
                vm = VirtualMachine.objects.get(id=user_vm.vm_id)
                if vm.server_id == server_id:
                    vms.append(VmDto(vm, user_vm))
            return Response(R.ok(VmDtoSerializer(vms, many=True).data))
        except Exception as e:
            return Response(R.failed('获取失败'))

    @action(methods=['post'], detail=False)
    def get_by_id(self, request):
        vm_id = request.data.get("vm_id")
        user_id = request.user.id
        try:
            user_vm = UserVm.objects.get(user_id=user_id, vm_id=vm_id)
            vm = VmDto(VirtualMachine.objects.get(id=vm_id), user_vm)
            server = Server.objects.get(id=vm.server_id)
            vm.ip = server.ip
            return Response(R.ok(VmDtoSerializer(vm).data))
        except Exception as e:
            return Response(R.failed('获取失败'))

    @action(methods=['post'], detail=False)
    def test_access(self, request):
        server_id = request.data.get('server_id')
        alias = request.data.get('alias')
        port = request.data.get('port')
        username = request.data.get('username')
        password = request.data.get('password')
        target_server = Server.objects.filter(id=server_id).first()
        if target_server is None:
            return Response(R.failed('不存在该服务器'))
        vm_service = VmService(target_server.ip, port, alias, username, password)
        auth = vm_service.auth()
        if auth:
            return Response(R.ok('测试连接成功'))
        else:
            return Response(R.failed('测试链接失败'))

    @action(methods=['post'], detail=False)
    def add(self, request):
        user_id = request.user.id
        server_id = request.data.get('server_id')
        port = request.data.get('port')
        alias = request.data.get('alias')
        username = request.data.get('username')
        password = request.data.get('password')
        # 判断该虚拟机是否已经被该用户添加过
        target_vm = VirtualMachine.objects \
            .filter(server_id=server_id, port=port, username=username,
                    password=password).first()
        if target_vm is not None and \
                UserVm.objects.filter(user_id=user_id, vm_id=target_vm.id).count() > 0:
            return Response(R.failed('该虚拟机已经被添加过'))
        target_server = Server.objects.filter(id=server_id).first()
        if target_server is None:
            return Response(R.failed('不存在该服务器'))
        vm_service = VmService(target_server.ip, port, alias, username, password)
        auth = vm_service.auth()
        if auth:
            try:
                with transaction.atomic():
                    if target_vm is None:
                        target_vm = VirtualMachine.objects \
                            .create(server_id=server_id, port=port, username=username,
                                    password=password)
                        target_vm.save()
                        # 开启监测
                        vm_service.vm_id = target_vm.id
                        VirtualMachineView.vms[target_vm.id] = vm_service
                        vm_service.begin_check()
                    new_user_vm = UserVm.objects \
                        .create(user_id=request.user.id, vm_id=target_vm.id,
                                alias=alias)
                    new_user_vm.save()
                    target_user_server = UserServer.objects \
                        .filter(user_id=user_id, server_id=target_server.id).first()
                    if target_user_server is None:
                        return Response(R.failed('您还未添加该服务器'))
                    target_user_server.vm_count += 1
                    target_user_server.save()

                    return Response(R.ok('添加成功'))
            except Exception:
                return Response(R.failed('添加失败，请重试'))
        else:
            return Response(R.failed('添加失败，请检查信息'))

    @action(methods=['delete'], detail=False)
    def delete(self, request):
        user_id = request.user.id
        vm_id = request.data.get('vm_id')
        try:
            with transaction.atomic():
                UserVm.objects.get(user_id=user_id, vm_id=vm_id).delete()

                target_vm = VirtualMachine.objects.get(id=vm_id)
                target_user_server = UserServer.objects \
                    .filter(user_id=user_id, server_id=target_vm.server_id).first()
                if target_user_server is None:
                    return Response(R.failed('您还未添加该服务器'))
                target_user_server.vm_count -= 1
                target_user_server.save()

                if UserVm.objects.filter(vm_id=vm_id).count() == 0:
                    target_vm.delete()
                    # 停止监测
                    vm_service = VirtualMachineView.vms[target_vm.id]
                    vm_service.stop_check = True
                    VirtualMachineView.vms.pop(target_vm.id)

                return Response(R.ok('删除成功'))
        except Exception:
            return Response(R.failed('删除失败，请重试'))

    @action(methods=['post'], detail=False)
    def get_data_5s(self, request):
        vm_id = request.data.get('vm_id')
        if vm_id not in VirtualMachineView.vms:
            return Response(R.failed('该虚拟机不存在'))
        vm_service = VirtualMachineView.vms[vm_id]
        data = vm_service.get_points_5s()
        return HttpResponse(data)

    @action(methods=['post'], detail=False)
    def get_data_30m(self, request):
        vm_id = request.data.get('vm_id')
        if vm_id not in VirtualMachineView.vms:
            return Response(R.failed('该虚拟机不存在'))
        vm_service = VirtualMachineView.vms[vm_id]
        data = vm_service.get_points_30m(vm_id)
        return HttpResponse(data)

    @action(methods=['post'], detail=False)
    def save_alias(self, request):
        user_id = request.user.id
        vm_id = request.data.get('vm_id')
        new_alias = request.data.get('new_alias')
        target_vm = UserVm.objects.get(user_id=user_id, vm_id=vm_id)
        target_vm.alias = new_alias
        target_vm.save()
        return Response(R.ok('修改成功'))
