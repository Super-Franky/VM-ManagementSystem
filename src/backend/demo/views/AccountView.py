from django.contrib.auth.models import User
from django.db import transaction
from pymysql import IntegrityError
from rest_framework import viewsets, permissions
from rest_framework.decorators import action
from rest_framework.response import Response

from demo.common.R import R
from demo.models import Account
from demo.serializers import AccountSerializer

from demo.views.public_func import account_logout, account_login, add_permission


class AccountView(viewsets.ModelViewSet):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    permission_classes = [permissions.AllowAny]

    @action(methods=['post'], detail=False)
    def register(self, request):
        stage = 'creating User'
        try:
            with transaction.atomic():
                username = request.data.get('username')
                password = request.data.get('password')
                new_user = User.objects.create_user(username=username, password=password)
                stage = 'creating account'
                new_account = Account.objects.create(user=new_user)
                add_permission(new_account)
                new_account.save()
                return Response(R.ok(AccountSerializer(new_account).data))
        except IntegrityError:
            return Response(R.failed('Duplicate username'))
        except Exception as e:
            return Response(R.failed(stage + 'failed.' + str(e)))

    @action(methods=['post'], detail=False)
    def login(self, request):
        try:
            if account_login(request):
                return Response(R.ok(request.data.get('username')))
            else:
                return Response(R.failed('密码错误'))
        except Exception as e:
            return Response(R.failed(str(e)))

    @action(methods=['post'], detail=False)
    def logout(self, request):
        try:
            account_logout(request)
            if request.user.is_anonymous:
                return Response(R.ok(request.data.get('username')))
            else:
                return Response(R.failed('退出失败'))
        except Exception as e:
            return Response(R.failed(str(e)))
