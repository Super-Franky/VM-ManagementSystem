# 账户登录函数
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, Permission


def account_login(request):
    username = request.data.get('username')
    password = request.data.get('password')
    user = authenticate(username=username, password=password)
    if user:
        login(request, user)
        return True
    else:
        return False


# 账户登出函数
def account_logout(request):
    logout(request)
    request.session.clear()


# 添加权限
def add_permission(target_account):
    permission_list = []
    for needed_permission in ['account', 'server', 'virtualmachine', 'userserver', 'uservm']:
        permission_list += Permission.objects.filter(codename__contains=needed_permission).all()
    for permission in permission_list:
        target_account.user.user_permissions.add(permission)
