import json

from django.http import HttpResponse
from rest_framework.response import Response

from demo.common.R import R

try:
    from django.utils.deprecation import MiddlewareMixin  # Django 1.10.x
except ImportError:
    MiddlewareMixin = object  # Django 1.4.x - Django 1.9.x

allow_list = ['/admin/login', '/demo/account/login', '/demo/account/register', '/demo/account/logout']


def is_have(request):
    for element in allow_list:
        if request.path.startswith(element):
            return True
    return False


class SimpleMiddleware(MiddlewareMixin):

    def process_request(self, request):
        if is_have(request):
            return None
        if request.user.is_authenticated:
            return None
        else:
            response = HttpResponse(content_type='application/json;charset=utf-8')
            response.content = json.dumps(R.not_login(), ensure_ascii=False)
            return response

    def process_response(self, request, response):
        return response
