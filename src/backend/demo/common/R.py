SUCCESS = 0
FAILED = 1
NOT_LOGIN = 2


class R:
    """
    code
    data
    msg
    """

    @staticmethod
    def ok(data):
        return {
            'code': SUCCESS,
            'data': data,
        }

    @staticmethod
    def failed(msg):
        return {
            'code': FAILED,
            'msg': msg,
        }

    @staticmethod
    def not_login():
        return {
            'code': NOT_LOGIN,
            'msg': '用户未登录',
        }
