import paramiko
import time


def connectHost(ip, port, username, password):
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(ip, port, username, password)
    except Exception:
        raise Exception
    return ssh


def check(ip, port, username, password):
    ssh = connectHost(ip, port, username, password)
    while True:
        # 查看网络使用率,方式为间隔1秒分别获取两次数据，用来计算速度
        # rx是接收速率，tx是发送速率
        net_query = "ip -s link |sed -n '10p;12p'| awk '{print $1}'"
        stdin, stdout, stderr = ssh.exec_command(net_query)
        net1 = stdout.readlines()
        net1_rx = int(net1[0])
        net1_tx = int(net1[1])
        time.sleep(1)
        stdin, stdout, stderr = ssh.exec_command(net_query)
        net2 = stdout.readlines()
        net2_rx = int(net2[0])
        net2_tx = int(net2[1])
        net_rx = str(round((net2_rx - net1_rx) / 1024, 2))
        net_tx = str(round((net2_tx - net1_tx) / 1024, 2))
        # 查看cpu使用率
        cpu_query = "vmstat|sed  '1d'|sed  '1d'|awk '{print $15}'"
        stdin, stdout, stderr = ssh.exec_command(cpu_query)
        cpu = stdout.readlines()
        cpu_usage = str(round((100 - int(cpu[0])), 2))
        # 查看内存使用率
        mem = "cat /proc/meminfo|sed -n '1p;3p'|awk '{print $2}'"
        stdin, stdout, stderr = ssh.exec_command(mem)
        mem = stdout.readlines()
        mem_total = round(int(mem[0]))
        mem_total_free = round(int(mem[1]))
        mem_usage = str(round(((mem_total - mem_total_free) / mem_total) * 100, 2))
        # 查看磁盘使用率
        disk_query = "df --total |grep '^total' | awk '{print$3,$4}"
        stdin, stdout, stderr = ssh.exec_command(disk_query)

        disk = stdout.readlines()[0].split(" ")
        disk_used = int(disk[0])
        disk_available = int(disk[1])
        disk_usage = str(round((disk_used / (disk_used + disk_available) * 100), 2))
        print(net_rx, net_tx, cpu_usage, mem_usage, disk_usage)
        # 由于前面计算网络流量时sleep了1秒，故此处本来应该sleep4秒
        # 但是由于ssh的网络传输延迟，查询各个数据时需要时间，这个时间大概是1秒
        # 故此处sleep3秒以满足每5秒获取一次数据的需求
        time.sleep(3)
