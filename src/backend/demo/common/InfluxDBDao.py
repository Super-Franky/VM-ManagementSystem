from influxdb import InfluxDBClient
import time


class InfluxDBDao:
    def __init__(self, host, port, database):
        self.client = InfluxDBClient(host, port)
        self.client.switch_database(database)

    def add_data_to_influxdb(self, mem_usage, cpu_usage, disk_usage, net_rx, net_tx, vm_id):
        fields = {"mem_usage": mem_usage, 'cpu_usage': cpu_usage, 'disk_usage': disk_usage, 'net_rx': net_rx, 'net_tx': net_tx}

        content = [
            {
                "measurement": "vm_5s",
                "fields": fields
            }
        ]
        tags = {
            'vm_id': vm_id
        }
        self.client.write_points(content, tags=tags)

    def query_data_from_influxdb_5s(self, vm_id):
        return self.client.query("SELECT * FROM vm_5s where vm_id='" + str(vm_id) + "'")

    def query_data_from_influxdb_30m(self, vm_id):
        return self.client.query("SELECT * FROM autogen.vm_30m where vm_id='" + str(vm_id) + "'")
