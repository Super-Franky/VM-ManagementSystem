from django.db import models
from django.contrib.auth.models import User


class Account(models.Model):
    user = models.OneToOneField(to=User, on_delete=models.CASCADE, null=True)


class Server(models.Model):
    ip = models.CharField(max_length=15)
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)


class VirtualMachine(models.Model):
    server_id = models.IntegerField(null=False)
    port = models.IntegerField(null=False)
    username = models.CharField(max_length=50, default='default')
    password = models.CharField(max_length=50)


class UserServer(models.Model):
    user_id = models.IntegerField(null=False)
    server_id = models.IntegerField(null=False)
    alias = models.CharField(max_length=50, default='alias')
    vm_count = models.IntegerField(null=False, default=0)


class UserVm(models.Model):
    user_id = models.IntegerField(null=False)
    vm_id = models.IntegerField(null=False)
    alias = models.CharField(max_length=50, default='alias')
