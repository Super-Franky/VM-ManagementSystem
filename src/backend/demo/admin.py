from django.contrib import admin
from .models import Account, Server, VirtualMachine, UserServer, UserVm

admin.site.register(Account)
admin.site.register(Server)
admin.site.register(VirtualMachine)
admin.site.register(UserServer)
admin.site.register(UserVm)
