import paramiko
from paramiko.ssh_exception import AuthenticationException


class ServerService:
    def __init__(self, ip, alias, username, password):
        self.ssh = paramiko.SSHClient()
        self.ip = ip
        self.alias = alias
        self.username = username
        self.password = password

    def auth(self):
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            self.ssh.connect(self.ip, 22, self.username, self.password, timeout=5)
        except Exception:
            return False
        finally:
            self.ssh.close()
        return True
