from threading import Thread

import paramiko
import time

from demo.common.InfluxDBDao import InfluxDBDao
from demo.common.check import connectHost


class VmService:
    def __init__(self, ip, port, alias, username, password, vm_id=-1):
        self.ssh = paramiko.SSHClient()
        self.ip = ip
        self.port = port
        self.alias = alias
        self.username = username
        self.password = password
        self.vm_id = vm_id

        self.stop_check = False
        self.influxDB = InfluxDBDao('influxdb', 8086, 'vm_data')
        # self.influxDB = InfluxDBDao('127.0.0.1', 8086, 'vm_data')

    def auth(self):
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            self.ssh.connect(self.ip, self.port, self.username, self.password, timeout=5)
        except Exception:
            return False
        finally:
            self.ssh.close()
        return True

    def begin_check(self):
        check_thread = Thread(target=self.check, args=(self.ip, self.port, self.username, self.password), daemon=True)
        check_thread.start()

    def check(self, ip, port, username, password):
        ssh = connectHost(ip, port, username, password)
        while not self.stop_check:
            # 查看网络使用率,方式为间隔1秒分别获取两次数据，用来计算速度
            # rx是接收速率，tx是发送速率
            net_query = "ip -s link |sed -n '10p;12p'| awk '{print $1}'"

            stdin, stdout, stderr = ssh.exec_command(net_query)
            net1 = stdout.readlines()
            net1_rx = int(net1[0])
            net1_tx = int(net1[1])
            time.sleep(1)
            stdin, stdout, stderr = ssh.exec_command(net_query)
            net2 = stdout.readlines()
            net2_rx = int(net2[0])
            net2_tx = int(net2[1])
            net_rx = str(round((net2_rx - net1_rx) / 1024, 2))
            net_tx = str(round((net2_tx - net1_tx) / 1024, 2))
            # 查看cpu使用率
            cpu_query = "vmstat|sed  '1d'|sed  '1d'|awk '{print $15}'"
            stdin, stdout, stderr = ssh.exec_command(cpu_query)
            cpu = stdout.readlines()
            cpu_usage = str(round((100 - int(cpu[0])), 2))
            # 查看内存使用率
            mem = "cat /proc/meminfo|sed -n '1p;3p'|awk '{print $2}'"
            stdin, stdout, stderr = ssh.exec_command(mem)
            mem = stdout.readlines()
            mem_total = round(int(mem[0]))
            mem_total_free = round(int(mem[1]))
            mem_usage = str(round(((mem_total - mem_total_free) / mem_total) * 100, 2))
            # 查看磁盘使用率
            disk_query = "df --total |grep '^total' | awk '{print$3,$4}'"
            stdin, stdout, stderr = ssh.exec_command(disk_query)
            disk = stdout.readlines()[0].split(" ")
            disk_used = int(disk[0])
            disk_available = int(disk[1])
            disk_usage = str(round((disk_used / (disk_used + disk_available) * 100), 2))
            print(net_rx, net_tx, cpu_usage, mem_usage, disk_usage)
            # 将数据写入 influxDB
            self.influxDB.add_data_to_influxdb(mem_usage, cpu_usage, disk_usage, net_rx, net_tx, self.vm_id)
            # 由于前面计算网络流量时sleep了1秒，故此处本来应该sleep4秒
            # 但是由于ssh的网络传输延迟，查询各个数据时需要时间，这个时间大概是1秒
            # 故此处sleep3秒以满足每5秒获取一次数据的需求
            time.sleep(3)

    def get_points_5s(self):
        data = self.influxDB.query_data_from_influxdb_5s(self.vm_id)
        print(data)
        return data

    def get_points_30m(self):
        data = self.influxDB.query_data_from_influxdb_30m(self.vm_id)
        print(data)
        return data
