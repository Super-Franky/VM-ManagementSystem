from rest_framework import serializers

from demo.dto.ServerDto import ServerDto
from demo.dto.VmDto import VmDto
from demo.models import Account, Server, VirtualMachine


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = '__all__'


class ServerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Server
        fields = '__all__'


class VirtualMachineSerializer(serializers.ModelSerializer):
    class Meta:
        model = VirtualMachine
        fields = '__all__'


class ServerDtoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServerDto
        fields = '__all__'


class VmDtoSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmDto
        fields = '__all__'
