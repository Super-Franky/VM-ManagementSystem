from django.db import models


class VmDto(models.Model):
    id = models.IntegerField(null=False, primary_key=True)
    server_id = models.IntegerField(null=False)
    port = models.IntegerField(null=False)
    username = models.CharField(max_length=50, default='default')
    password = models.CharField(max_length=50)
    alias = models.CharField(max_length=50, default='alias')
    ip = models.CharField(max_length=15, null=True)

    def __init__(self, vm, user_vm):
        self.id = user_vm.vm_id
        self.server_id = vm.server_id
        self.port = vm.port
        self.username = vm.username
        self.password = vm.password
        self.alias = user_vm.alias
