from django.db import models


class ServerDto(models.Model):
    id = models.IntegerField(null=False, primary_key=True)
    ip = models.CharField(max_length=15)
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    user_id = models.IntegerField(null=False)
    alias = models.CharField(max_length=50, default='alias')
    vm_count = models.IntegerField(null=False, default=0)

    def __init__(self, server, user_server):
        self.id = user_server.server_id
        self.user_id = user_server.user_id
        self.ip = server.ip
        self.alias = user_server.alias
        self.username = server.username
        self.password = server.password
        self.vm_count = user_server.vm_count
