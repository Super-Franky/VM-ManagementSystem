from django.urls import path, include

from rest_framework import routers


from demo.views.ServerView import ServerView
from demo.views.AccountView import AccountView
from demo.views.VirtualMachineView import VirtualMachineView

router = routers.DefaultRouter()
router.register('account', AccountView)
router.register('server', ServerView)
router.register('vm', VirtualMachineView)

urlpatterns = [
    path('', include(router.urls)),
]
